<?php
class Controllers_Users extends RestController {
	private $_con;
	private $payload;
	private function DBconnect(){
		$this->_con=mysqli_connect("localhost","liadni","72973","liadni_educationProgramDay");
			

	}
	public function get() {
		$this->DBconnect();
		//var_dump($this->request['params']['id']);
		//die();
		if (isset($this->request['params']['id'])){
			if(!empty($this->request['params']['id'])) {
				$sql="SELECT * FROM users WHERE id=?";				
				$stmt = $this->_con->prepare($sql);
				$stmt->bind_param("s",$this->request['params']['id']);
				$stmt->execute();
				$stmt->store_result();				
				$stmt->bind_result($id, $name,$email);
				$stmt->fetch();
				$result = ['id'=>$id,'name'=>$name,'email'=>$email];
				$this->response = array('result' =>$result );
				$this->responseStatus = 200;
			} else {
				$sql="SELECT * FROM users";
				$usersSqlResult = mysqli_query($this->_con, $sql);
				$users = [];
				while ($user = mysqli_fetch_array($usersSqlResult)){
				$users[] = ['name' => $user['name'], 'email' => $user['email']];
				}
				$this->response = array('result' =>$users);
				$this->response = array('result' =>$users);
				$this->responseStatus = 200;
				}	
		}else{
			$this->response = array('result' =>'Wrong parameters for users' );
			$this->responseStatus = 200;			
		}
	}
	public function post() {
		$this->DBconnect();
		$json = json_decode($this->request['params']['payload'],true);
		$name = $json[name];
		$email = $json[email];
		$sql="INSERT INTO `users`(`id`, `name`, `email`) VALUES ('NULL','$name','$email')";
		$sqlInsert = mysqli_query ($this->_con, $sql);
			////////
		$last_id = $this->_con->insert_id; // זו הפונקציה שנותנת את האחרון
		   /////////
		$this->response =  $last_id;
		$this->responseStatus = 201;
	}
	public function put() {
		$this->response = array('result' => 'no put implemented for users');
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('result' => 'no delete implemented for users');
		$this->responseStatus = 200;
	}
}
